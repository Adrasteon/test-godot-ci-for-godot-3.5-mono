using Godot;

public partial class RotationScript : Sprite
{
	[Export]
	public float Speed = 300; // How fast the player will move (pixels/sec).



	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		var velocity = Vector2.Zero;

		if (Input.IsKeyPressed((int)KeyList.Right))
		{
			velocity += Vector2.Right;
		}

		if (Input.IsKeyPressed((int)KeyList.Left))
		{
			velocity += Vector2.Left;
		}

		if (Input.IsKeyPressed((int)KeyList.Down))
		{
			velocity += Vector2.Down;
		}

		if (Input.IsKeyPressed((int)KeyList.Up))
		{
			velocity += Vector2.Up;
		}

		Position += ((float)delta * Speed) * velocity;
	}
}
